#!/bin/bash

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

exercise=ex_$1
binary=$exercise.bin
tester="$exercise.sh"
output="output.test"

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cc tests/$exercise/*.c -o tests/$exercise/$binary &> /dev/null
    ./tests/$exercise/$tester tests/$exercise/$binary &> "./tests/${exercise}_REF"
else
    sudo -u student cc ./$exercise/*.c -o $binary
    sudo -u student ./tests/$exercise/$tester ./$binary &> output.test    
    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
